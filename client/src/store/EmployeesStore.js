import {makeAutoObservable} from "mobx";


export default class EmployeesStore {
    constructor() {
        this._types = [
            {id: 1, name: 'Все сотрудники'},
            {id: 2, name: 'SP-1'},
            {id: 3, name: 'SP-2'},
            {id: 4, name: 'SP-3'},
        ];
        this._selectedType = {id: 1, name: 'Все сотрудники'};
        makeAutoObservable(this)
    }
    setTypes(types) {
        this._types = types
    }
    setSelectedType(type) {
        this._selectedType = type
    }

    get types() {
        return this._types
    }
    get selectedType() {
        return this._selectedType
    }
};

