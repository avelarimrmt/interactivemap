import React, {useContext, useState} from 'react';
import '../styles/map.css';
import '../styles/employees.css';
import '../styles/global.css';
import {Context} from "../index";
import {observer} from "mobx-react-lite";
import SideBar from "../components/SideBar";
import Profile from "../components/Profile";
import SidebarEmpDirection from "../components/SidebarEmpDirection";
import {API_ROOT, LOGIN_ROUTE} from "../utils/consts";
import EditMyDataModal from "../components/EditMyDataModal";
import EditDataEmp from "../components/EditDataEmp";
import DeleteEmp from "../components/DeleteEmp";
import AddEmp from "../components/AddEmp";


const Employees = observer(() => {
    const {user} = useContext(Context);
    const [listEmp, setListEmp] = useState([{}]);
    const [showMore, setShowMore] = useState(false);
    const [modalIsOpenEdit,setIsOpenEdit] = useState(false);
    const [modalIsOpenDelete,setIsOpenDelete] = useState(false);
    const [modalIsOpenAdd,setIsOpenAdd] = useState(false);
    const [idEmp, setIdEmp] = useState(0);

    const getEmployees = async () => {
        const response = await fetch(
            `https://localhost:5001/api/employees/by-employees`,
            {
                method: "GET",
                headers: {"Accept": "application/json"}
            });
        if (response.ok === true) {
            const employees = await response.json();
            let list = [];
            for (let employee of employees) {
                let data = {
                    lastName: employee.lastName,
                    firstName: employee.firstName,
                    photo: employee.photoId,
                    positionName: employee.position.name,
                    id: employee.id,
                    email: employee.emailAddress,
                    direction: employee.direction,
                    status: employee.status.name,
                    statusId: employee.status.id,
                };
                list.push(data);
            }
            setListEmp(list);
        }
    };
    /*getEmployees();*/

    const [employee, setEmployee] = useState({});

    const getEmployee = async (id) => {
        const response = await fetch(
            `${process.env.REACT_APP_API_URL}api/employees/by-em-id/${id}`,
            {
                method: "GET",
                headers: {"Accept": "application/json"}
            });
        let data = {};
        if (response.ok === true) {
            const emp = await response.json();
            data = {
                lastName: emp.lastName,
                firstName: emp.firstName,
                id: emp.id,
                middleName: emp.middleName,
                position: emp.position.name,
                email: emp.emailAddress,
                direction: emp.direction,
                team: emp.team,
            };
        }
        setEmployee(data);
    };

    return (
        <div className="w-screen h-screen">
            <header className="z-10 fixed bg-white w-screen top-0">
                <div className={`flex w-full h-full justify-end`}>
                    <div className="flex items-center">
                        {user.isAdmin ?
                            <div className="mr-16">
                                <button type="button" id="addElement" className="text-white text-center" onClick={()=>setIsOpenAdd(true)}>
                                    <span className="">+ Новый элемент</span>
                                </button>
                            </div>
                            : null}

                        <Profile/>
                    </div>
                </div>
            </header>

            <SideBar/>

            <SidebarEmpDirection/>

            <div id="all-emp" className="flex items-center relative">
                <div className="all-emp-wrapper flex flex-col bg-white pl-10">

                    <div className="flex justify-between items-center pl-10 pt-16 mb-10">
                        <div className="search-form flex justify-self-start  ">
                            <div className="dropdown relative inline-block">
                                <input className="relative" id="textInput" autoComplete="off" type="text"
                                       placeholder="Найти сотрудника..."/>
                                <ul id="dropDown"
                                    className={` absolute top-10 flex flex-col items-start bg-white`}>

                                </ul>
                                <div className="h-full absolute right-2.5 top-0 flex items-center">
                                    <button type="button" id="clearButton" className="hidden">
                                        <svg className="btn-svg " width="26" height="26" viewBox="0 0 26 26" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <g className="fill-clear-search">
                                                <path
                                                    d="M6.36396 6.36407L12.7279 12.728M19.0919 19.092L12.7279 12.728M12.7279 12.728L6.36396 19.092M12.7279 12.728L19.0919 6.36407"
                                                    stroke="#464646" strokeWidth="2" strokeLinejoin="round"/>
                                            </g>
                                        </svg>
                                    </button>
                                </div>

                            </div>
                            <div className="btn-search-clear flex justify-center h-full">
                                <button className="flex justify-center items-center" type="button" id="searchButton">
                                    <svg className="btn-svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path className="fill-clear-search"
                                              d="M11 19C15.4183 19 19 15.4183 19 11C19 6.58172 15.4183 3 11 3C6.58172 3 3 6.58172 3 11C3 15.4183 6.58172 19 11 19Z"
                                              stroke="#464646" strokeWidth="3" strokeLinecap="round"
                                              strokeLinejoin="round"/>
                                        <path d="M21.0004 20.9999L16.6504 16.6499" stroke="#464646" strokeWidth="3"
                                              strokeLinecap="round" strokeLinejoin="round"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        {!user.isAdmin ?
                            <div className="mr-16">
                                <button type="button" id="addElement" className="text-white text-center" onClick={()=>setIsOpenAdd(true)}>
                                    <span className="">+ Новый сотрудник</span>
                                </button>
                            </div>
                            : null}
                    </div>


                    <table className="table">
                        <thead>
                        <tr>
                            <th className="th-text th-avatar"/>
                            <th className="th-text th-fio">Фамилия Имя</th>
                            <th className="th-text th-pos">Должность</th>
                            <th className="th-text th-status">Статус</th>
                            <th className="th-text th-email">E-mail</th>
                            <th className="th-text th-more"/>
                        </tr>
                        </thead>
                        <tbody>
                        {listEmp.map((value) => {
                            return <tr className="w-full">
                                <td className="pl-3 pt-6 pb-6 pr-3 td-text flex justify-center">
                                    <div className="avatar flex justify-center">
                                        <img src={API_ROOT + `/photos/${value.photo}.png`}
                                             alt="avatar"/>
                                    </div>
                                </td>
                                <td className="td-fio td-text">{value.firstName + " " + value.lastName}</td>
                                <td className="td-pos td-text">{value.positionName}</td>
                                <td className="td-status td-text">
                                    <div className="flex h-full items-center">
                                        <div className="flex">
                                            <img src={API_ROOT + `/status/${value.statusId}.svg`}
                                                 alt="status" className="mr-3"/></div>
                                        {value.status}
                                    </div>
                                </td>
                                <td className="td-email td-text">{value.email}</td>
                                {!user.isAdmin ?
                                    <td className="td-more relative">
                                        <button type="button" className="points-more"
                                                onBlur={(e)=>{setTimeout(()=>
                                                {e.target.nextElementSibling.classList.remove("block");
                                                e.target.nextElementSibling.classList.add("hidden")}, 600)}}
                                                onClick={(e)=>
                                                {e.target.nextElementSibling.classList.add("block");
                                                e.target.nextElementSibling.classList.remove("hidden") }}/>
                                        <div className={`hidden menu menu-more absolute top-16 right-0 bg-white`}>
                                            <ul>
                                                <li className="p-4 flex justify-self-start  text-edit" onClick={()=>{setIdEmp(value.id); getEmployee(value.id); setIsOpenEdit(true)}}>
                                                    Редактировать
                                                </li>
                                                <li className="p-4 flex justify-self-start  text-delete" onClick={()=>{setIdEmp(value.id); getEmployee(value.id); setIsOpenDelete(true)}}>
                                                    Удалить
                                                </li>
                                            </ul>
                                        </div>
                                    </td> : null}
                            </tr>
                        })}
                        </tbody>
                    </table>
                </div>
            </div>
            <EditDataEmp isOpen={modalIsOpenEdit}
                         onRequestClose={() => {
                             setIsOpenEdit(false);
                         }}
                         firstName={employee.firstName}
                         lastName={employee.lastName}
                         middleName={employee.middleName}
                         direction={employee.direction}
                         team={employee.team}
                         position={employee.position}
                         email={employee.email}
                         id={idEmp}
                         initial={true}
                         overlayClassName="overlay overlayModal"
                         className="modal edit w-auto"/>
            <DeleteEmp isOpen={modalIsOpenDelete}
                         onRequestClose={() => {
                             setIsOpenDelete(false);
                         }}
                         firstName={employee.firstName}
                         lastName={employee.lastName}
                         id={idEmp}
                         overlayClassName="overlay overlayModal"
                         className="modal delete w-auto"/>
            <AddEmp isOpen={modalIsOpenAdd}
                       onRequestClose={() => {
                           setIsOpenAdd(false);
                       }}
                       firstName={employee.firstName}
                       lastName={employee.lastName}
                       id={idEmp}
                       overlayClassName="overlay overlayModal"
                       className="modal edit w-auto"/>
        </div>
    );
});
export default Employees;
