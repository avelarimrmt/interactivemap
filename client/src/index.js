import React,  {createContext} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import UserStore from "./store/UserStore";
import EmployeesStore from "./store/EmployeesStore";

export const Context = createContext(null);
ReactDOM.render(
  <Context.Provider value={{
      user: new UserStore(),
      employees: new EmployeesStore(),
  }}>
    <App />
  </Context.Provider>,
  document.getElementById('root')
);

