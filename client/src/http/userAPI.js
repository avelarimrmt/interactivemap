import {$host} from "./index";
import sha256 from "js-sha256";

export const registration = async (email, password) => {
    return await $host.post('api/account/get-token', {email, password});
};

export const login = async (email, password) => {
    const hashValue = sha256(password);
    const formData = new FormData();
    formData.append("email", email);
    formData.append("password", hashValue);

    const response = await fetch("https://localhost:5001/api/account/get-token", {
        method: "POST",
        headers: {"Accept": "application/json"},
        body: formData
    });

    if (response.ok === true) {
        const data = await response.json();
        return data;
    } else {
        console.log("Error: ", response.status);
        return null;
    }
};
