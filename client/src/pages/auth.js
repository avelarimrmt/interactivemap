import React, {useContext, useState} from "react";
import '../styles/auth.css';
import {LOGIN_ROUTE, MAP_ROUTE, REGISTRATION_ROUTE} from "../utils/consts";
import {NavLink, useLocation, useHistory} from "react-router-dom";
import {login} from "../http/userAPI";
import {observer} from "mobx-react-lite";
import {Context} from "../index";

const Auth = observer(() => {
    const {user} = useContext(Context);
    const location = useLocation();
    const history = useHistory();
    const isLogin = location.pathname === LOGIN_ROUTE;
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const click = async () => {
        let data;
        if (isLogin) {
            data = await login(email, password);
            if (data) {
                user.setUser({firstName: data.data.firstName, lastName: data.data.lastName});
                sessionStorage.setItem("accessToken", data.access_token);
                sessionStorage.setItem("idEmp", data.data.id_emp);
                sessionStorage.setItem("firstName", data.data.firstName);
                sessionStorage.setItem("lastName", data.data.lastName);
                sessionStorage.setItem("photo", data.data.photo);
                history.push(MAP_ROUTE);
            }
        }
    };

    return (
        <div className="h-full flex justify-center items-center">
            <div className={`${isLogin ? "wrap-form-login" : "wrap-form-reg"} wrap-form bg-white`}>
                <form className="h-full w-full flex flex-col align-center justify-center">
                    <div className="wrap-title-login w-full flex justify-between items-center mb-5">
                        <h2 className="title-login">{isLogin ? 'Вход' : 'Регистрация'}</h2>
                        {isLogin ? <NavLink to={REGISTRATION_ROUTE}>
                                <button type="button" className="subtitle-login text-sm">Регистрация</button>
                            </NavLink> :
                            <NavLink to={LOGIN_ROUTE}>
                                <button type="button" className="subtitle-login text-sm">Вход</button>
                            </NavLink>}
                    </div>

                    <div className="hidden uncorrected-data flex p-3">
                        <svg className="mr-3" width="20" height="20" viewBox="0 0 20 20" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M8.5749 3.21659L1.51656 14.9999C1.37104 15.2519 1.29403 15.5377 1.29322 15.8287C1.2924 16.1197 1.3678 16.4059 1.51192 16.6587C1.65603 16.9115 1.86383 17.1222 2.11465 17.2698C2.36547 17.4174 2.65056 17.4967 2.94156 17.4999H17.0582C17.3492 17.4967 17.6343 17.4174 17.8851 17.2698C18.136 17.1222 18.3438 16.9115 18.4879 16.6587C18.632 16.4059 18.7074 16.1197 18.7066 15.8287C18.7058 15.5377 18.6288 15.2519 18.4832 14.9999L11.4249 3.21659C11.2763 2.97168 11.0672 2.76919 10.8176 2.62866C10.568 2.48813 10.2863 2.41431 9.9999 2.41431C9.71345 2.41431 9.43184 2.48813 9.18223 2.62866C8.93263 2.76919 8.72345 2.97168 8.5749 3.21659V3.21659Z"
                                stroke="#EF5A5A" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                            <path d="M10 7.5V10.8333" stroke="#EF5A5A" strokeWidth="2" strokeLinecap="round"
                                  strokeLinejoin="round"/>
                            <path d="M10 13.3334V13.75" stroke="#EF5A5A" strokeWidth="2" strokeLinecap="round"
                                  strokeLinejoin="round"/>
                        </svg>
                        <p className="text-sm text-center">Неверный логин или пароль</p>
                    </div>

                    {isLogin ? null : <div className="description flex p-3">
                        <p className="text-sm">Для регистрации Вам необходимо ввести E-mail, который внес
                            администратор в базу</p>
                    </div>}

                    <div className="wrap-input w-full mt-5">
                        <div className="flex w-full justify-start">
                            <span className="title-fields mb-1">E-mail</span>
                        </div>
                        <div className="flex w-full">
                            <div className={`name-input flex mb-5 ${isLogin ? "w-full" : "w-7/12"}`}>
                                <input id="login-input" type="text" name="username" value={email}
                                       onChange={e => setEmail(e.target.value)}/>
                            </div>
                            {isLogin ? null : <div className="flex flex-grow ml-2">
                                <div className="w-full ">
                                    <button type="button" id="check-email" className="w-full text-center">Проверить
                                    </button>
                                </div>
                            </div>}
                        </div>

                        <div className="flex w-full justify-start">
                            <span className="title-fields mb-1">Пароль</span>
                        </div>
                        <div className={`pass-input w-7/12 flex mb-4 ${isLogin ? "w-full" : "w-7/12"}`}>
                            <input type="password" id="password-input" name="password" value={password}
                                   onChange={e => setPassword(e.target.value)}/>
                            <a href="#" className="password-control"/>
                        </div>

                        {isLogin ? null :
                            <div>
                                <div className="flex w-full justify-start">
                                    <span className="title-fields mb-1">Повторите пароль</span>
                                </div>
                                <div className="pass-input w-7/12 flex mb-4 w-7/12">
                                    <input type="password" id="password-input" name="password"/>
                                    <a href="#" className="password-control"/>
                                </div>
                            </div>
                        }

                        {isLogin ? <div className="remember-wrapper flex w-full">
                            <label className="remember relative cursor-pointer">Запомнить меня
                                <input type="checkbox" className="absolute h-0 w-0 cursor-pointer opacity-0"/>
                                <span className="checkmark absolute top-0 left-0"/>
                            </label>
                        </div> : null}


                        <div className="btn-wrapper flex">
                            <button id="submit-login" type="button" onClick={click}
                                    className="w-full text-lg text-center text-white login-btn">{isLogin ? "Войти" : "Зарегистрироваться"}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
});
export default Auth;


