import React from 'react';
import {observer} from "mobx-react-lite";
import ReactModal from "react-modal";
import '../styles/Components/DeleteEmp.css';

const DeleteEmp = observer(({...rest}) => {
    const deleteEmployee = async ()=> {
        const response = await fetch(
            `${process.env.REACT_APP_API_URL}api/employees/delete-emp/${rest.id}`,
            {
                method: "POST",
                headers: {"Accept": "application/json"}
            });
        if (response.ok === true) {
            console.log(true);
        }
    };
        return (
            <ReactModal {...rest} parentSelector={() => document.querySelector('#root')}>
                <div className="modal-wrapper relative bg-white  w-full h-full">
                    <form className="flex items-center h-full w-full flex-col justify-start">
                        <div className="flex justify-between w-full pl-10 pr-8 pt-10 mb-8">
                            <div className="title-edit mt-6">Удаление сотрудника</div>
                        </div>
                        <div className="flex items-start flex-col w-full pl-10">
                            <div className="mb-10">
                                <p className="desc-delete">
                                    Вы собираетесь удалить сотрудника <span className="font-bold">{rest.lastName+" "+rest.firstName}</span> из базы.
                                    После удаления, он не сможет пользоваться сервисом.
                                </p>
                            </div>
                            <div className="flex justify-end w-full">
                                <div className="">
                                    <button type="button" id="close-btn" className="w-full text-center" onClick={rest.onRequestClose}>Отмена</button>
                                </div>
                                <div className="flex ml-4 mr-8">
                                    <button id="submit-delete" type="submit"
                                            className="w-full text-lg text-center text-white login-btn"
                                            onClick={() => {deleteEmployee(); setTimeout(rest.onRequestClose, 100)}}>Удалить
                                    </button>
                                </div>
                            </div>

                        </div>


                    </form>
                </div>
            </ReactModal>
        );
    })
;

export default DeleteEmp;
