export const MAP_ROUTE = '/map';
export const LOGIN_ROUTE = '/login';
export const REGISTRATION_ROUTE = '/registration';
export const EMPLOYEES_ROUTE = '/employees';
export const API_ROOT = 'https://localhost:5001';
