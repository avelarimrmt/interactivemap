﻿namespace OfficeMap.Models
{
    public partial class Role
    {
        public Role()
        {
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}