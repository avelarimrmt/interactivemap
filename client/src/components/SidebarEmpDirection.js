import React, {useContext} from 'react';
import {Context} from "../index";
import {observer} from "mobx-react-lite";
import '../styles/Components/SideBar.css';

const SidebarEmpDirection = observer(() => {
    const {employees} = useContext(Context);
    return (
        <div id="sidebar-emp-dir" className="z-10 fixed flex items-center">
            <div className="sidebar-emp-wrapper flex flex-col justify-center items-center bg-white">
                <div className="flex h-full w-full pt-16 pl-6 flex-col">
                    {employees.types.map(type =>
                        (type.name === "Все сотрудники") ?
                            <div>
                                <div className="relative w-full flex">
                                    <div
                                        style={{cursor: 'pointer'}}
                                        className={`${(type.id === employees.selectedType.id) ? "active" : null} text-def-sidebar`}
                                        key={type.id}
                                        onClick={() => employees.setSelectedType(type)}

                                    >
                                        {type.name}
                                        <div className="active-section absolute right-0 top-0">
                                        </div>
                                    </div>
                                </div>
                                <div className="text-directions mt-4 mb-1">НАПРАВЛЕНИЯ:</div>
                            </div>
                            :
                            <div className="flex relative w-full">
                                <div
                                    style={{cursor: 'pointer'}}
                                    className={`${(type.id === employees.selectedType.id) ? "active" : null} text-def-sidebar mb-2`}
                                    key={type.id}
                                    onClick={() => employees.setSelectedType(type)}

                                >
                                    {type.name}
                                    <div className="active-section absolute right-0 top-0">
                                    </div>
                                </div>
                            </div>
                    )}
                </div>
            </div>
        </div>
    );
});

export default SidebarEmpDirection;
