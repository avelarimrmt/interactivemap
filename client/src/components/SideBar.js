import React from 'react';
import {NavLink, useLocation} from "react-router-dom";
import {EMPLOYEES_ROUTE, MAP_ROUTE} from "../utils/consts";
import ButtonEmp from "./ButtonEmp";
import '../styles/Components/SideBar.css';
import ButtonMap from "./ButtonMap";

const SideBar = () => {
    const location = useLocation();
    const isMap = location.pathname === MAP_ROUTE;
    const isEmployees = location.pathname === EMPLOYEES_ROUTE;

    return (
        <section id="sidebar" className="z-10 fixed flex items-center">
            <div className="sidebar-wrapper flex flex-col justify-center items-center bg-white">
                <NavLink exact activeClassName="active" className="w-full" to={MAP_ROUTE}>
                    <ButtonMap  color={`${isMap ? "#4263EB" : "#ACB5BD"}`}/>
                </NavLink>

                <NavLink activeClassName="active" className="w-full" to={EMPLOYEES_ROUTE}>
                    <ButtonEmp color={`${isEmployees ? "#4263EB" : "#ACB5BD"}`}/>
                </NavLink>
            </div>
        </section>
    );
};

export default SideBar;
