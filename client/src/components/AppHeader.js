import React, {useContext, useState} from 'react';
import {Context} from "../index";
import {API_ROOT, MAP_ROUTE} from "../utils/consts";
import '../styles/Components/AppHeader.css';
import {observer} from "mobx-react-lite";

const AppHeader = observer(({isExistSearchInput}) => {
    const {user} = useContext(Context);
    const [inputValue, setInputValue] = useState('');
    const [showList, setShowList] = useState(false);
    let currentEmployees;
    const [listOfEmployees, setListOfEmployees] = useState([]);
    const onInput = async (event) => {
        if (event.target.value === '') {
            /* очищаем список с задержкой,
             * задержка нужна при большом количестве событий oninput,
             * иначе обработчик события oninput с непустой строкой сработает позже
             */
            setTimeout(()=>setListOfEmployees([]), 500);
        }
        else {
            const response = await fetch(
                `${process.env.REACT_APP_API_URL}api/employees/starts-with/${event.target.value}`,
                {
                    method: "GET",
                    headers: {"Accept": "application/json"}
                });
            if (response.ok === true) {
                const employees = await response.json();
                let list = [];
                currentEmployees = employees;
                setListOfEmployees([]);
                for (let employee of employees) {
                    list.push(`${employee.lastName} ${employee.firstName}`);
                }
                setListOfEmployees(list);
            }
        }
    };



    const [showResults, setShowResults] = useState(false);

    return (
        <header className="z-10 fixed bg-white w-screen top-0">
            <div className={`flex w-full h-full ${isExistSearchInput ? "justify-between" : "justify-end"}`}>
                {isExistSearchInput ?
                    <div className="search-wrapper flex justify-self-start items-center ml-20">
                        <div className="search-form flex justify-self-start">
                            <div className="dropdown relative inline-block">
                                <input className="relative" id="textInput" autoComplete="off" type="text" value={inputValue}
                                       onChange={e => setInputValue(e.target.value)}
                                       onBlur={() => setShowList(false)}
                                       onFocus={() => setShowList(true)}
                                       onInput={onInput}
                                       placeholder="Найти рабочее место сотрудника..."/>
                                <ul id="dropDown" className={`absolute top-10 flex flex-col items-start bg-white ${showList ? "block" : "hidden"}`}>

                                    {listOfEmployees.map((value, index) => {
                                        return <li key={index}>{value}</li>
                                    })}
                                </ul>
                                <div className="h-full absolute right-2.5 top-0 flex items-center">
                                    <button type="button" id="clearButton" className={`${inputValue !== '' ? "block" : "hidden"}`}
                                    onClick={() => setInputValue('')}>
                                        <svg className="btn-svg " width="26" height="26" viewBox="0 0 26 26" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <g className="fill-clear-search">
                                                <path
                                                    d="M6.36396 6.36407L12.7279 12.728M19.0919 19.092L12.7279 12.728M12.7279 12.728L6.36396 19.092M12.7279 12.728L19.0919 6.36407"
                                                    stroke="#464646" strokeWidth="2" strokeLinejoin="round"/>
                                            </g>
                                        </svg>
                                    </button>
                                </div>

                            </div>
                            <div className="btn-search-clear flex justify-center h-full">
                                <button className="flex justify-center items-center" type="button" id="searchButton"
                                onClick={()=>setShowResults(true)}>
                                    <svg className="btn-svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path className="fill-clear-search"
                                              d="M11 19C15.4183 19 19 15.4183 19 11C19 6.58172 15.4183 3 11 3C6.58172 3 3 6.58172 3 11C3 15.4183 6.58172 19 11 19Z"
                                              stroke="#464646" strokeWidth="3" strokeLinecap="round"
                                              strokeLinejoin="round"/>
                                        <path d="M21.0004 20.9999L16.6504 16.6499" stroke="#464646" strokeWidth="3"
                                              strokeLinecap="round" strokeLinejoin="round"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                    : null}

                <div className="flex items-center">
                    {isExistSearchInput && user.isAdmin ?
                        <div className="mr-16">
                            <button type="button" id="addElement" className="text-white text-center">
                                <span className="">+ Новый элемент</span>
                            </button>
                        </div>
                        : null}

                    <div className="profile-wrapper flex justify-self-start items-center mr-14">
                            <button type="button" id="btnMore" className="flex items-center">
                                <div className="avatar mr-2.5">
                                    <img src={API_ROOT + `/photos/${sessionStorage.getItem("photo")}.png`} alt="avatar"/>
                                </div>
                                <div className="initials mr-2.5">
                                    <p>{user.user.firstName+ " " +user.user.lastName} </p>
                                </div>
                                <svg width="13" height="7" viewBox="0 0 13 7" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 1L6.5 6L1 1" stroke="#ACB5BD" strokeWidth="2" strokeLinecap="round"
                                          strokeLinejoin="round"/>
                                </svg>
                            </button>
                    </div>
                </div>
            </div>
        </header>
    );
});
export default AppHeader;
