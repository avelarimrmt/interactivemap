import React, {useContext, useState} from 'react';
import {API_ROOT, LOGIN_ROUTE} from "../utils/consts";
import {Context} from "../index";
import {observer} from "mobx-react-lite";
import {useHistory} from "react-router-dom";
import EditMyDataModal from "./EditMyDataModal";

const Profile = observer(() => {
    const {user} = useContext(Context);
    const [showMenu, setShowMenu] = useState(false);
    const history = useHistory();
    const [modalIsOpen,setIsOpen] = useState(false);

    return (
        <div className="profile-wrapper flex justify-self-start items-center mr-14 ">
            <button type="button" id="btnMore" className="flex items-center relative" onBlur={()=>setShowMenu(false)} onClick={()=>setShowMenu(true)}>
                <div className="avatar mr-2.5">
                    <img src={API_ROOT + `/photos/${sessionStorage.getItem("photo")}.png`}
                         alt="avatar"/>
                </div>
                <div className="initials mr-2.5">
                    <p>{user.user.firstName+ " " +user.user.lastName}</p>
                </div>
                <svg width="13" height="7" viewBox="0 0 13 7" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 1L6.5 6L1 1" stroke="#ACB5BD" strokeWidth="2" strokeLinecap="round"
                          strokeLinejoin="round"/>
                </svg>
                <div className={`${showMenu ? "block" : "hidden"} menu absolute top-12 right-0 bg-white`}>
                    <ul>
                        <li className="p-4" onClick={()=>setIsOpen(true)}>
                            Редактировать профиль
                        </li>
                        <li className="p-4" onClick={()=>history.push(LOGIN_ROUTE)}>
                            Выйти
                        </li>

                    </ul>
                </div>
            </button>

            <EditMyDataModal isOpen={modalIsOpen}
                             onRequestClose={() => {
                                 setIsOpen(false);
                             }}
                             overlayClassName="overlay overlayModal"
                             className="modal w-auto"/>
        </div>
    );
});

export default Profile;
