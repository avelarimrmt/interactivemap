import {EMPLOYEES_ROUTE, LOGIN_ROUTE, MAP_ROUTE, REGISTRATION_ROUTE} from "./utils/consts";
import Employees from "./pages/employees";
import Auth from "./pages/auth";
import Map from "./pages/map";

export const authRoutes = [
    {
        path: MAP_ROUTE,
        Component: Map
    },
    {
        path: EMPLOYEES_ROUTE,
        Component: Employees
    },
];

export const publicRoutes = [
    {
        path: LOGIN_ROUTE,
        Component: Auth
    },
    {
        path: REGISTRATION_ROUTE,
        Component: Auth
    },
];
