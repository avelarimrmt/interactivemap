import React, {useContext, useState} from 'react';
import {observer} from "mobx-react-lite";
import ReactModal from "react-modal";
import '../styles/Components/EditDataEmp.css';

const AddEmp = observer(({...rest}) => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [middleName, setMiddleName] = useState('');
    const [direction, setDirection] = useState('');
    const [team, setTeam] = useState('');
    const [position, setPosition] = useState('');
    const [email, setEmail] = useState('');

    const cleanInputs = () => {
        setLastName('');
        setFirstName('');
        setMiddleName('');
        setTeam('');
        setPosition('');
        setEmail('');
        setDirection('');
    };

    const addEmployee = async (event) => {
        event.preventDefault();
        let object = {FirstName: firstName, LastName: lastName, MiddleName: middleName, Direction: direction,
        PositionId: 1, Team: team, EmailAddress: email, Id: 55, StatusId: 6, PhoneNumber: "8(798) 256-28-64",
            RoleId: 0, PhotoId: 1};
        const formData = new FormData();
        Object.keys(object).forEach(key => formData.append(key, object[key]));
        const response = await fetch(`https://localhost:5001/api/employees/add-emp`, {
            method: "POST",
            headers: {"Accept": "application/json"},
            body: formData
        });

        if (response.ok) {
            const data = await response.json();
        } else {
            console.log("Error: ", response.status);
            return null;
        }
        cleanInputs();
    };
        return (
            <ReactModal {...rest} parentSelector={() => document.querySelector('#root')}>
                <div className="modal-wrapper relative bg-white  w-full h-full">
                    <form onSubmit={addEmployee} className="flex items-center h-full w-full flex-col justify-start">
                        <div className="flex justify-between w-full pl-10 pr-8 pt-10 mb-10">
                            <div className="title-edit mt-6">Добавление сотрудника</div>
                            <button type="button" className="close-btn" onClick={rest.onRequestClose}>
                                <svg className="btn-svg svg-close" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g>
                                        <path
                                            d="M4.92898 4.92893L12 12M19.0711 19.0711L12 12M12 12L4.92898 19.0711M12 12L19.0711 4.92893"
                                            stroke="#ACB5BD" strokeWidth="2" strokeLinejoin="round"/>
                                    </g>
                                </svg>
                            </button>
                        </div>
                        <div className="flex items-start flex-col w-full pl-10 overflow-y-auto" >
                            <div className="w-full mb-4 surname">
                                <div className="flex w-full justify-start">
                                    <span className="title-fields mb-1">Фамилия <span
                                        className="text-red-400">*</span></span>
                                </div>
                                <div className="flex w-full">
                                    <div className={`input-wrapper flex w-full`}>
                                        <input id="surname-input" type="text" name="lastName"
                                               value={lastName}
                                               onChange={e => setLastName(e.target.value)}/>
                                    </div>
                                </div>
                            </div>
                            <div className="w-full mb-4 name">
                                <div className="flex w-full justify-start">
                                    <span className="title-fields mb-1">Имя <span
                                        className="text-red-400">*</span></span>
                                </div>
                                <div className="flex w-full">
                                    <div className={`input-wrapper flex w-full`}>
                                        <input id="name-input" type="text" name="firstName"
                                               value={firstName}
                                               onChange={e => setFirstName(e.target.value)}/>
                                    </div>
                                </div>
                            </div>
                            <div className="w-full mb-6 middlename">
                                <div className="flex w-full justify-start">
                                    <span className="title-fields mb-1">Отчество <span
                                        className="text-red-400">*</span></span>
                                </div>
                                <div className="flex w-full">
                                    <div className={`input-wrapper flex w-full`}>
                                        <input id="middle-input" type="text" name="middleName"
                                               value={middleName}
                                               onChange={e => setMiddleName(e.target.value)}/>
                                    </div>
                                </div>
                            </div>
                            <div className="w-full mb-4 direction">
                                <div className="flex w-full justify-start">
                                    <span className="title-fields mb-1">Направление <span
                                        className="text-red-400">*</span></span>
                                </div>
                                <div className="flex w-full">
                                    <div className={`input-wrapper flex w-full`}>
                                        <input id="dir-input" type="text" name="directName"
                                               value={direction}
                                               onChange={e => setDirection(e.target.value)}/>
                                    </div>
                                </div>
                            </div>
                            <div className="w-full mb-4 team">
                                <div className="flex w-full justify-start">
                                    <span className="title-fields mb-1">Команда <span
                                        className="text-red-400">*</span></span>
                                </div>
                                <div className="flex w-full">
                                    <div className={`input-wrapper flex w-full text-base`}>
                                        <input id="team-input" type="text" name="teamName"
                                               value={team}
                                               onChange={e => setTeam(e.target.value)}/>
                                    </div>
                                </div>
                            </div>
                            <div className="w-full mb-6 position">
                                <div className="flex w-full justify-start">
                                    <span className="title-fields mb-1">Должность <span
                                        className="text-red-400">*</span></span>
                                </div>
                                <div className="flex w-full">
                                    <div className={`input-wrapper flex w-full text-base`}>
                                        <input id="pos-input" type="text" name="posName"
                                               value={position}
                                               onChange={e => setPosition(e.target.value)}/>
                                    </div>
                                </div>
                            </div>
                            <div className="w-full mb-12 email">
                                <div className="flex w-full justify-start">
                                    <span className="title-fields mb-1">Email <span
                                        className="text-red-400">*</span></span>
                                </div>
                                <div className="flex w-full">
                                    <div className={`input-wrapper flex w-full text-base`}>
                                        <input id="mail-input" type="text" name="mail"
                                               value={email === '' ? setEmail(rest.email) : email}
                                               onChange={e => setEmail(e.target.value)}/>
                                    </div>
                                </div>
                            </div>

                            <div className="flex justify-end w-full mb-8">
                                <div className="">
                                    <button type="button" id="close-btn" className="w-full text-center" onClick={rest.onRequestClose}>Отмена</button>
                                </div>
                                <div className="flex ml-4 mr-8">
                                    <button id="submit-save" type="submit"
                                            className="w-full text-lg text-center text-white login-btn"
                                            onClick={() => setTimeout(rest.onRequestClose, 300)}>Добавить
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </ReactModal>
        );
    })
;

export default AddEmp;
