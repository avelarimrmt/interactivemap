import React, {useContext, useState} from 'react';
import {Context} from "../index";
import {observer} from "mobx-react-lite";
import ReactModal from "react-modal";

const EditMyDataModal = observer(({...rest}) => {
    const {user} = useContext(Context);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const patchEmployee = async (event) => {
        event.preventDefault();
        let object = {FirstName: firstName, LastName: lastName};
        const formData = new FormData();
        Object.keys(object).forEach(key => formData.append(key, object[key]));
        const response = await fetch(`https://localhost:5001/api/employees/by-emp-id/${sessionStorage.getItem("idEmp")}`, {
            method: "PATCH",
            headers: {"Accept": "application/json"},
            body: formData
        });

        if (response.ok) {
            const data = await response.json();
            user.setUser(data);
        } else {
            console.log("Error: ", response.status);
            return null;
        }
    };
    return (
        <ReactModal {...rest} parentSelector={() => document.querySelector('#root')}>
            <div className="flex bg-white items-center">
                <form onSubmit={patchEmployee} className="flex h-full w-full bg-white flex-col justify-end">
                    <input className="name border-2 border-black" type="text" name="firstName" value={firstName}
                           onChange={e => setFirstName(e.target.value)}/>
                    <input className="surname border-2 border-black" type="text" name="lastName" value={lastName}
                           onChange={e => setLastName(e.target.value)}/>
                    <button type="submit" onClick={()=>setTimeout(rest.onRequestClose, 100)}>Сохранить</button>
                </form>
            </div>
        </ReactModal>
    );
});

export default EditMyDataModal;
